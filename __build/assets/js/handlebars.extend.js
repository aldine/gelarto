Handlebars.registerHelper( 'yes_no', value => typeof value === 'boolean' ? ( value ? 'Yes' : 'No' ) : value );

Handlebars.registerHelper( 'readable', str => new Handlebars.SafeString(str.replace(/\_/g, ' ').replace(/and/g, '&amp;')) );

Handlebars.registerHelper( 'uppercase', str => str.toUpperCase() );
