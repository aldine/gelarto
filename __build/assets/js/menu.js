var 
    ll = new LazyLoad({
        elements_selector: '.lazy'
    }),
    block_template = document.querySelector('#block_template')?Handlebars.compile(document.querySelector('#block_template').innerHTML):null,
    lightbox_template = document.querySelector('#lightbox_template')?Handlebars.compile(document.querySelector('#lightbox_template').innerHTML):null,
    close = () => document.querySelector('.lightbox').classList.remove('show'),
    print = () => {
        var body = document.body.innerHTML;
        document.body.innerHTML = document.querySelector('.lightbox .box').innerHTML;
        window.print();
        document.body.innerHTML = body;
        close();
    },
    openLightbox = _this => {

        var code = _this.getAttribute('data-code'),
            lightbox = document.querySelector('.lightbox'),
            box = lightbox.querySelector('.box');

        box.style.top = (window.pageYOffset || document.documentElement.scrollTop) + 'px';

        box.innerHTML = lightbox_template(menu[code]);

        document.querySelector('.lightbox .close').addEventListener('click', close);
        document.querySelector('.lightbox .print').addEventListener('click', print);

        lightbox.classList.add('show');

    },
    menu = {},
    items = document.querySelector('.menu .items'),
    build = () => {
        fetch(site.baseurl + '/assets/menu.json')
            .then(resp => resp.json())
            .then( data => {

                items.innerHTML = '';
                
                data.each( item => {

                    item.site = site;
                    menu[item.code] = item;

                    var tmp = document.createElement( 'div' );
            
                    tmp.innerHTML = block_template(item).trim();
                
                    items.appendChild(tmp.childNodes[0]);

                });

                // LazyLoad
                if ( ll )
                    ll.update();
            })
            .catch(console.error);
    };

if ( document.getElementById('block_template') ) {
    build();

    document.querySelector('.lightbox .background').addEventListener('click', close);
}
