var eachUb8v6 = function (callback) {
    var v, c = 0, len = (typeof(this)=='object'?Object.getOwnPropertyNames(this):this).length;
    for (var i in this)
        if (c++ < len)
            callback.apply(v = this[i], [v, i]);
};

Object.defineProperty(Object.prototype, 'each', { value: eachUb8v6, enumberable: false });
HTMLCollection.prototype.each = NodeList.prototype.each = Array.prototype.each = eachUb8v6;
