const { src, dest, series, parallel, watch, lastRun } = require('gulp');
const pug = 			require('gulp-pug');
const uglify = 			require('gulp-uglify-es').default;
const concat = 			require('gulp-concat');

const iconfont = 		require('gulp-iconfont');
const iconfontCss = 	require('gulp-iconfont-css');

const imagemin = 		require('gulp-imagemin');
const webp = 			require('imagemin-webp');
const extReplace = 		require('gulp-ext-replace');

const autocrop = 		require('gulp-autocrop');
const pathcrop = 		require('gulp-pathcrop');

const rename = 			require('gulp-rename');























function pugTask(cb) {
	src('__build/**/*.pug')
		.pipe(pug({ pretty: true }))
		.pipe(dest('./'))
		.on('finish', cb);
}

function watchPugTask(cb) {
	watch('__build/**/*.pug', pugTask);

	cb();
}

function jsTask(cb) {
	src([
		'__build/assets/js/**/*.js',
		'!__build/assets/js/**/*.min.js'
		], { sourcemaps: true })
		.pipe(concat('bundle.js'))
		.pipe(uglify())
		.pipe(dest('./assets/js/', { sourcemaps: '.' }))
		.on('finish', cb);
}

function jsMinTask(cb) {
	src('__build/assets/js/**/*.min.js')
		.pipe(dest('./assets/js/'))
		.on('finish', cb);
}

function watchJsTask(cb) {
	watch('__build/assets/js/**/*.*', parallel(jsTask, jsMinTask));

	cb();
}





















function fontTask (cb) {

	var fontName = 'icons';

	src('__build/assets/font/*.svg')
		.pipe(imagemin([
			imagemin.svgo({
				plugins: [
					{ cleanupNumericValues: 0 }
				]
			})
		]))
		.pipe(iconfontCss({
			fontName: fontName,
			path: '__build/assets/font/.icons.template.css',
			targetPath: '../css/icons.css',
			fontPath: '../fonts/',
			cacheBuster: Math.round(+new Date / 1e4)
		}))
		.pipe(iconfont({
			fontName: fontName
		}))
		.pipe(dest('assets/fonts'))
		.on('finish', cb);

}

function imageTask (cb) {
	src([
		'__build/assets/images/**/*.{jpg,jpeg,png}',
		'!__build/assets/images/responsive/**/*.*',
		'!__build/assets/images/background.jpg',
		'!__build/assets/images/menu/**/*.*'], { since: lastRun(imageTask, 1000) })
		.pipe(pathcrop())
		.pipe(imagemin())
		.pipe(dest('./assets/images/'))
		.pipe(imagemin([
			webp({
				quality: 90
			})
		]))
		.pipe(extReplace('.webp'))
		.pipe(dest('./assets/images'))
		.on('finish', cb);
}

function svgTask (cb) {
	src('__build/assets/images/**/*.svg', { since: lastRun(svgTask, 1000) })
		.pipe(imagemin([
			imagemin.svgo({
				plugins: [
					{ cleanupNumericValues: 0 }
				]
			})
		]))
		.pipe(dest('./assets/images/'))
		.on('finish', cb);
}

function iconTask (cb) {
	const sizes = [32, 64, 68, 76, 120, 128, 152];
	let count = 0;
	sizes.forEach(function (size) {
		src('__build/assets/icons/**/*.png', { since: lastRun(iconTask, 1000) })
			.pipe(autocrop({
				height: size,
				width: size
			}))
			.pipe(rename({
				basename: size
			}))
			.pipe(imagemin())
			.pipe(dest('./assets/icons'))
			.on('finish', () => {
				count+=1;
				if(count === sizes.length)
					cb();
			})
	})
}


function responsiveImageTask (cb) {
	const sizes = [650,420,350];
	let count = 0;
	sizes.forEach(function(size) {
		src('__build/assets/images/responsive/**/*.{jpg,jpeg,png}')
			.pipe(autocrop({
				ratio: '325:244',
				width: size
			}))
			.pipe(rename({
				prefix: size + '_'
			}))
			.pipe(imagemin())
			.pipe(dest('./assets/images/responsive'))
			.pipe(imagemin([
				webp({
					quality: 90
				})
			]))
			.pipe(extReplace('.webp'))
			.pipe(dest('./assets/images/responsive'))
			.on('finish', () => {
				count+=1;
				if(count === sizes.length)
					cb();
			})
	});
}


function responsiveBackgroundTask (cb) {
	const sizes = [2000,1500,1000,800,500,400];
	let count = 0;
	sizes.forEach(function(size) {
		src('__build/assets/images/background.{jpg,jpeg,png}', { since: lastRun(responsiveBackgroundTask, 1000) })
			.pipe(autocrop({
				width: size
			}))
			.pipe(rename({
				suffix: '_' + size + 'w'
			}))
			.pipe(imagemin([
				imagemin.jpegtran({progressive: true})
			]))
			.pipe(dest('./assets/images'))
			.pipe(imagemin([
				webp({
					quality: 90
				})
			]))
			.pipe(extReplace('.webp'))
			.pipe(dest('./assets/images'))
			.on('finish', () => {
				count+=1;
				if(count === sizes.length)
					cb();
			});
	});
}


function menuImageTask (cb) {
	const sizes = [350];
	let count = 0;
	sizes.forEach(function(size) {
		src('__build/assets/images/menu/**/*.{jpg,jpeg,png}', { since: lastRun(menuImageTask, 1000) })
			.pipe(autocrop({
				width: size,
				height: size
			}))
			.pipe(rename({
				suffix: 'w' + size
			}))
			.pipe(imagemin())
			.pipe(dest('./assets/images/menu'))
			.on('finish', () => {
				count+=1;
				if(count === sizes.length)
					cb();
			});
	});
}


function polaroidImageTask (cb) {
	const sizes = [350];
	let count = 0;
	sizes.forEach(function(size) {
		src('__build/assets/images/polaroid/**/*.{jpg,jpeg,png}', { since: lastRun(polaroidImageTask, 1000) })
			.pipe(autocrop({
				width: size,
				height: size
			}))
			.pipe(imagemin([
				imagemin.jpegtran({progressive: true})
			]))
			.pipe(dest('./assets/images/polaroid'))
			.pipe(imagemin([
				webp({
					quality: 90
				})
			]))
			// .pipe(rename({
			// 	suffix: 'w' + size
			// }))
			.pipe(extReplace('.webp'))
			.pipe(imagemin())
			.pipe(dest('./assets/images/polaroid'))
			.on('finish', () => {
				count+=1;
				if(count === sizes.length)
					cb();
			});
	});
}




exports.polaroid = polaroidImageTask;
exports.menu = menuImageTask;
exports.resp = responsiveImageTask;
exports.background = responsiveBackgroundTask;
exports.images = imageTask;
exports.font = fontTask;
exports.svg = svgTask;
exports.watch = parallel(watchPugTask, watchJsTask);
exports.default = parallel(pugTask, jsTask, jsMinTask, fontTask, svgTask, imageTask, iconTask, responsiveImageTask, responsiveBackgroundTask, menuImageTask, polaroidImageTask);
